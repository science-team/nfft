#!/bin/sh
# Copyright 2015 Ghislain Antony Vaillant
#
# This file is part of the autopkgtest testsuite for NFFT.

set -e

set -x
if [ "${AUTOPKGTEST_TMP}" = "" ] ; then
  AUTOPKGTEST_TMP=$(mktemp -d /tmp/${pkg}-test.XXXXXX)
  trap "rm -rf ${AUTOPKGTEST_TMP}" 0 INT QUIT ABRT PIPE TERM
fi

cp ./examples/nfft/simple_test*.in "$AUTOPKGTEST_TMP"
cd "$AUTOPKGTEST_TMP"

echo "Non-threaded examples."

# Strip hard-coded define from source file.
sed -e '/@NFFT_PRECISION_MACRO@/d' simple_test.c.in > simple_test.c

# Build and run for single precision.
gcc -Wall -DNFFT_PRECISION_SINGLE -o simple_test simple_test.c `pkg-config --cflags --libs nfft3f`
echo "build: OK (single precision)"
[ -x simple_test ] && ./simple_test
echo "run: OK (single precision)"

# Build and run for double precision.
gcc -Wall -DNFFT_PRECISION_DOUBLE -o simple_test simple_test.c `pkg-config --cflags --libs nfft3`
echo "build: OK (double precision)"
[ -x simple_test ] && ./simple_test
echo "run: OK (double precision)"

echo "Multi-threaded examples."

# Strip hard-coded define from source file.
sed -e '/@NFFT_PRECISION_MACRO@/d' simple_test_threads.c.in > simple_test_threads.c

# Build and run for single precision.
gcc -Wall -DNFFT_PRECISION_SINGLE -o simple_test_threads simple_test_threads.c -lnfft3f -lnfft3f_threads -lfftw3f_threads
echo "build: OK (single precision)"
[ -x simple_test_threads ] && ./simple_test_threads
echo "run: OK (single precision)"

# Build and run for double precision.
gcc -Wall -DNFFT_PRECISION_DOUBLE -o simple_test_threads simple_test_threads.c -lnfft3 -lnfft3_threads -lfftw3_threads
echo "build: OK (double precision)"
[ -x simple_test_threads ] && ./simple_test_threads
echo "run: OK (double precision)"
